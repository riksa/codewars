import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RangeExtraction {
    public static String rangeExtraction(int[] arr) {
        var acc = new ArrayList<ArrayList<Integer>>();
        return Arrays.stream(arr)
                .mapToObj(x -> new ArrayList<>(List.of(x)))
                .reduce(acc, RangeExtraction::accumulator, (a, b) -> a)
                .stream().map(RangeExtraction::listToString)
                .collect(Collectors.joining(","));
    }

    private static String listToString(ArrayList<Integer> integers) {
        if (integers.size() >= 3) {
            return String.format("%d-%d", integers.get(0), integers.get(integers.size() - 1));
        }
        return integers.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    private static ArrayList<ArrayList<Integer>> accumulator(ArrayList<ArrayList<Integer>> a, ArrayList<Integer> i) {
        System.out.println("a: " + a.toString());
        System.out.println("i: " + i.toString());
        if (a.size() > 0) {
            var last = a.get(a.size() - 1);
            var item = i.get(0);
            if (last.get(last.size() - 1) == item - 1) {
                last.add(item);
                return a;
            }
        }
        a.add(i);
        return a;
    }


}