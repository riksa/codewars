import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class MorseCodeDecoder {
    private static final Map<String, String> morseMap = Map.ofEntries(
            Map.entry("a", ".-"),
            Map.entry("b", "-..."),
            Map.entry("c", "-.-."),
            Map.entry("d", "-.."),
            Map.entry("e", "."),
            Map.entry("f", "..-."),
            Map.entry("g", "--."),
            Map.entry("h", "...."),
            Map.entry("i", ".."),
            Map.entry("j", ".---"),
            Map.entry("k", "-.-"),
            Map.entry("l", ".-.."),
            Map.entry("m", "--"),
            Map.entry("n", "-."),
            Map.entry("o", "---"),
            Map.entry("p", ".--."),
            Map.entry("q", "--.-"),
            Map.entry("r", ".-."),
            Map.entry("s", "..."),
            Map.entry("t", "-"),
            Map.entry("u", "..-"),
            Map.entry("v", "...-"),
            Map.entry("w", ".--"),
            Map.entry("x", "-..-"),
            Map.entry("y", "-.--"),
            Map.entry("z", "--.."),
            Map.entry("0", "-----"),
            Map.entry("1", ".----"),
            Map.entry("2", "..---"),
            Map.entry("3", "...--"),
            Map.entry("4", "....-"),
            Map.entry("5", "....."),
            Map.entry("6", "-...."),
            Map.entry("7", "--..."),
            Map.entry("8", "---.."),
            Map.entry("9", "----."),
            Map.entry(".", ".-.-.-"),
            Map.entry(",", "--..--"),
            Map.entry("?", "..--.."),
            Map.entry("!", "-.-.--"),
            Map.entry("sos", "...---...")
    );

    public static String decode(String morseCode) {
        // your brilliant code here, remember that you can access the preloaded Morse code table through MorseCode.get(code)
        return Arrays.stream(morseCode.trim().split("\\s")).sequential()
                .map(MorseCodeDecoder::morseToChar)
                .map(String::toUpperCase)
                .collect(Collectors.joining())
                .replace("  ", " ");
    }

    public static String morseToChar(String code) {
        System.out.printf("finding %s", code);
        return morseMap.entrySet().stream()
                .filter(entry -> Objects.equals(entry.getValue(), code))
                .findFirst()
                .map(Map.Entry::getKey)
                .orElse(" ");
    }
}