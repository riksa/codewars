import java.util.HashMap;

public class DuplicateEncoder {
    static String encode(String word) {
        if (word == null) {
            return null;
        }
        word = word.toLowerCase();
        var dupeMap = new HashMap<Integer, Boolean>();
        word.chars()
                .forEach(x -> dupeMap.put(x, dupeMap.containsKey(x)));
        return word.chars()
                .mapToObj(x -> dupeMap.get(x) ? ")" : "(")
                .reduce((a, b) -> a + b).orElseThrow();
    }
}
