module gitlab.com/riksa/codewars

go 1.15

require (
	github.com/nxadm/tail v1.4.5 // indirect
	github.com/onsi/ginkgo v1.14.2
	github.com/onsi/gomega v1.10.3
	golang.org/x/sys v0.0.0-20201022201747-fb209a7c41cd // indirect
)
