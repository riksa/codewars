package kata

import (
	"math"
)

func DigPow(n, p int) int {
	// your code
	var digits []int
	for rem := n; rem > 0; rem /= 10 {
		digits = append([]int{rem % 10}, digits...)
	}

	num := 0
	for i, n := range digits {
		num += int(math.Pow(float64(n), float64(p+i)))
	}

	if num%n == 0 {
		return num / n
	}

	return -1
}
