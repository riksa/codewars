package kata

import (
	"errors"
	"log"
)

var units = []string{"nulla", "egy", "kettő", "három", "négy", "öt", "hat", "hét", "nyolc", "kilenc"}

// HungarianNumeral generates the Hungarian numeral string for a given integer.
func HungarianNumeral(n int) string {
	thousands, err := thousands(n)
	if err != nil {
		log.Fatal(err)
	}
	if n%1000 == 0 && n > 0 {
		return thousands
	}

	lessThanThousand, err := lessThanThousand(n%1000, true)
	if err != nil {
		log.Fatal(err)
	}
	return thousands + lessThanThousand
}

func lessThanThousand(n int, lastDigit bool) (string, error) {
	suffix, err := mod100Part(n%100, lastDigit)
	if err != nil {
		return "", nil
	}
	hundreds := hundreds(n)
	if n%100 == 0 && n > 0 {
		return hundreds, nil
	}
	return hundreds + suffix, nil
}

func thousands(n int) (string, error) {
	num := n / 1000
	if num == 0 {
		return "", nil
	}
	if num == 1 {
		return "ezer", nil
	}
	lessThanThousand, err := lessThanThousand(num, false)
	if err != nil {
		return "", err
	}

	str := lessThanThousand + "ezer"

	if n%1000 != 0 && n > 2000 {
		return str + "-", nil
	}
	return str, nil
}

func hundreds(n int) string {
	num := (n / 100) % 10
	if num == 0 {
		return ""
	}
	if num == 1 {
		return "száz"
	}
	if num == 2 {
		return "kétszáz"
	}
	return units[num] + "száz"
}

func mod100Part(n int, lastDigit bool) (string, error) {
	if n < 0 || n >= 100 {
		return "", errors.New("out of range")
	}
	if n <= 9 {
		return singleDigit(n, lastDigit)
	}
	return doubleDigit(n, lastDigit)
}

func singleDigit(n int, lastDigit bool) (string, error) {
	if n < 0 || n >= 10 {
		return "", errors.New("out of range")
	}
	if n == 2 && !lastDigit {
		return "két", nil
	}
	return units[n], nil
}

func doubleDigit(n int, lastDigit bool) (string, error) {
	if n < 10 || n >= 100 {
		return "", errors.New("out of range")
	}
	single := n % 10
	if single != 0 {
		postfix, err := singleDigit(single, lastDigit)
		if err != nil {
			return "", err
		}
		prefix := doubleDigitPrefix(n)
		return prefix + postfix, nil
	}

	return doubleDigitPrefix(n), nil
}

func doubleDigitPrefix(n int) string {
	var Tens = []string{"tíz", "húsz", "harminc", "negyven", "ötven", "hatvan", "hetven", "nyolcvan", "kilencven"}
	//              	"  10     20      30         40         50       60        70          80           90    "
	tens := n / 10
	single := n % 10

	if single != 0 {
		if tens == 1 {
			return "tizen"
		}
		if tens == 2 {
			return "huszon"
		}
	}
	return Tens[tens-1]
}
