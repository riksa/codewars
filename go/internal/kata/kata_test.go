// TODO: replace with your own tests (TDD). An example to get you started is included below.
// Ginkgo BDD Testing Framework <http://onsi.github.io/ginkgo/>
// Gomega Matcher Library <http://onsi.github.io/gomega/>

package kata_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "gitlab.com/riksa/codewars/internal/kata"
)

var _ = Describe("snail tests function", func() {
	It("should resolve 3x3", func() {
		snailMap := [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
		Expect(Snail(snailMap)).To(Equal([]int{1, 2, 3, 6, 9, 8, 7, 4, 5}))
	})
})

func dotest(n, p int, exp int) {
	var ans = DigPow(n, p)
	Expect(ans).To(Equal(exp))
}

var _ = Describe("Test Example", func() {

	It("should handle basic cases", func() {
		dotest(89, 1, 1)
		dotest(92, 1, -1)
		dotest(695, 2, 2)
		dotest(46288, 3, 51)
	})
})

var _ = Describe("NbYear", func() {
	It("fixed tests", func() {
		Expect(NbYear(1500, 5, 100, 5000)).To(Equal(15))
		Expect(NbYear(1500000, 2.5, 10000, 2000000)).To(Equal(10))
		Expect(NbYear(1500000, 0.25, 1000, 2000000)).To(Equal(94))
		Expect(NbYear(1500000, 0.25, -1000, 2000000)).To(Equal(151))
	})
})

var _ = Describe("Hungarian Cardinal Numerals", func() {
	It("Hungarian Cardinal Numerals", func() {
		Expect(HungarianNumeral(0)).To(Equal("nulla"))
		Expect(HungarianNumeral(1)).To(Equal("egy"))
		Expect(HungarianNumeral(10)).To(Equal("tíz"))
		Expect(HungarianNumeral(12)).To(Equal("tizenkettő"))
		Expect(HungarianNumeral(21)).To(Equal("huszonegy"))
		Expect(HungarianNumeral(71)).To(Equal("hetvenegy"))
		Expect(HungarianNumeral(100)).To(Equal("száz"))
		Expect(HungarianNumeral(200)).To(Equal("kétszáz"))
		Expect(HungarianNumeral(354)).To(Equal("háromszázötvennégy"))
		Expect(HungarianNumeral(1000)).To(Equal("ezer"))
		Expect(HungarianNumeral(1040)).To(Equal("ezernegyven"))
		Expect(HungarianNumeral(6023)).To(Equal("hatezer-huszonhárom"))
		Expect(HungarianNumeral(23100)).To(Equal("huszonháromezer-száz"))
		Expect(HungarianNumeral(914754)).To(Equal("kilencszáztizennégyezer-hétszázötvennégy"))
	})
})
