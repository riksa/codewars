package kata

func NbYear(p0 int, percent float64, aug int, p int) (x int) {
	pop := float64(p0)
	for ; pop < float64(p); x++ {
		pop = pop*(1+percent/100) + float64(aug)
	}
	return
}
