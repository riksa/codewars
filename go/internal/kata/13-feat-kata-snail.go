package kata

func Snail(snailMap [][]int) (ret []int) {
	if len(snailMap) == 0 || len(snailMap[0]) == 0 {
		return []int{}
	}
	if snailMap == nil {
		return nil
	}

	for {
		// Top
		ret = append(ret, snailMap[0]...)
		snailMap = snailMap[1:]
		if len(snailMap) == 0 {
			return
		}

		// Right
		rowlast := len(snailMap[0]) - 1
		for i := range snailMap {
			row := snailMap[i]
			ret = append(ret, row[rowlast])
			snailMap[i] = snailMap[i][:rowlast]
		}

		// Bottom
		lastRow := len(snailMap)-1
		row := snailMap[lastRow]
		snailMap = snailMap[:lastRow]
		for i := len(row)-1 ; i >= 0 ; i-- {
			ret = append(ret, row[i])
		}
		if len(snailMap) == 0 {
			return
		}

		// Left
		for i := len(snailMap)-1 ; i >= 0 ; i-- {
			row := snailMap[i]
			ret = append(ret, row[0])
			snailMap[i] = snailMap[i][1:]
		}
	}
}
