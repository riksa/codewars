using System.Collections.Generic;
using System.Linq;

public class Kata
{
    public static string FirstNonRepeatingLetter(string s)
    {
        var chars = s.ToCharArray();
        return s
            .Select(char.ToUpperInvariant)
            .Aggregate(new Dictionary<char, bool>(), Reducer)
            .Where(x => x.Value)
            .Select(x => x.Key)
            .Select(x => LowerCaseMatches(chars, x))
            .Select(x => x.ToString())
            .FirstOrDefault() ?? "";
    }

    private static string LowerCaseMatches(IEnumerable<char> chars, char c)
    {
        return chars.FirstOrDefault(x => char.ToUpperInvariant(x) == c)
            .ToString();
    }

    private static Dictionary<char, bool> Reducer(Dictionary<char, bool> dict, char c)
    {
        dict[c] = !dict.ContainsKey(c);
        return dict;
    }
}