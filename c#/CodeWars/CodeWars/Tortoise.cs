﻿public class Tortoise
{
    /// <summary>
    /// Two tortoises named A and B must run a race. A starts with an average speed of 720 feet per hour. Young B knows she runs faster than A, and furthermore has not finished her cabbage.
    /// When she starts, at last, she can see that A has a 70 feet lead but B's speed is 850 feet per hour. How long will it take B to catch A?
    /// More generally: given two speeds v1 (A's speed, integer > 0) and v2 (B's speed, integer > 0) and a lead g (integer > 0) how long will it take B to catch A?
    /// The result will be an array [hour, min, sec] which is the time needed in hours, minutes and seconds (round down to the nearest second) or a string in some languages.
    /// If v1 >= v2 then return nil, nothing, null, None or {-1, -1, -1} for C++, C, Go, Nim, [] for Kotlin or "-1 -1 -1".
    /// </summary>
    /// <param name="v1">Tortoise A speed in feet / hour</param>
    /// <param name="v2">Tortoise B speed in feet / hour</param>
    /// <param name="g">Lead in feet</param>
    /// <returns></returns>
    public static int[] Race(int v1, int v2, int g)
    {
        if (v1 >= v2)
        {
            return null;
        }

        var speedDifference = v2 - v1;
        var secondsToCatch = 60 * 60 * g / speedDifference;
        var seconds = secondsToCatch % 60;
        var minutes = (secondsToCatch / 60) % 60;
        var hours = (secondsToCatch / 60 / 60);
        return new[] {hours, minutes, seconds};
    }
}