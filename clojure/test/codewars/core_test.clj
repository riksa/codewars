(ns codewars.core-test
  (:require [clojure.test :refer :all]
            [codewars.core :refer :all]))

(deftest sample-tests
  (is (= (dna-strand "ATCG") "TAGC")))
