(ns codewars.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn dna-strand [dna]
  (clojure.string/join (map {\A \T \T \A \G \C \C \G} (seq dna))))